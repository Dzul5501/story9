// JavaScript source code
  var count=0;
  var onGBook = false;
  var onManga = false;
  var ocountme = false;

  $(document).ready(function(){     
    callAjax("get-books/");
    setCounter(0);
});


function setCounter(cnt){
    $("#counter").html(cnt);
}

function createInformationBooks(title,authors,description) {
    var res="";
    res+="<div class='wrapper_text_inside'>";
    res+="<h3>"+title+"</h3>";
    res+="<p><b>"+authors+"</b>";
    res+="<br>"+description+"</p>";
    res+="</div>"
    return res;
}

function createFav(id) { 
    return "<i id='star-"+id+"' class='far fa-star' onClick='FavMe(`"+id+"`)' style='font-size:45px'></i>";
 }

 function FavMe(id) { 
    id = id+""
    if("rgb(33, 37, 41)" === $("#star-"+id).css('color') ){
        $("#star-"+id).css('color','rgb(249, 214, 34)');
        count++;
        setCounter(count);  
    } else if("rgb(249, 214, 34)" === $("#star-"+id).css('color') ){
        $("#star-"+id).css('color','rgb(33, 37, 41)');    
        count--;
        setCounter(count);
    }
    
  }

$('#btnStory').click(function(){
    console.log("click:btnStory");
    callAjax("get-books/");
    onGBook = true;
    onManga = false;
    ocountme = false;
})
function setDataTable() { 
    console.log("DataTable");
    $('#table-result').DataTable();
 }
function callAjax(param) { 
    if(param === "get-books/" & onGBook){
        return;
    }
	$.ajax({
        url:"/"+param,
        dataType:"json",
        success: function (response) { 
            console.log("success");
            var lists = response["data"];
            var append = "";
            for (i = 0; i < lists.length; i++){
                append+="<tr>";
                append+="<td>"+(i+1)+"</td>";
                append+="<td>"+lists[i]['title']+"</td>";//kolom T A D
                append+="<td>"+lists[i]['authors']+"</td>";//kolom T A D 
                append+="<td style='text-align:justify'>"+lists[i]['description']+"</td>";//kolom T A D
                append+="<td>"+"<img class='cover-image' src="+lists[i]['cover']+">"+"</td>";//kolom cover
                append+="<td>"+createFav(lists[i]['id'])+"</td>";//kolom kolom favorite
                append+="<tr>";
            }
            $('#result').html(append);    
        }

    })
 }